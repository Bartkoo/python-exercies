print("Welcome to the Love Calculator!")
name1 = input("What is your name? \n")
name2 = input("What is their name? \n")

FirstNameToLower = name1.lower()
SecondNameToLower = name2.lower()

TrueCount = 0
TrueCount += FirstNameToLower.count("t")
TrueCount += FirstNameToLower.count("r")
TrueCount += FirstNameToLower.count("u")
TrueCount += FirstNameToLower.count("e")
TrueCount += SecondNameToLower.count("t")
TrueCount += SecondNameToLower.count("r")
TrueCount += SecondNameToLower.count("u")
TrueCount += SecondNameToLower.count("e")

LoveCount = 0
LoveCount += FirstNameToLower.count("l")
LoveCount += FirstNameToLower.count("o")
LoveCount += FirstNameToLower.count("v")
LoveCount += FirstNameToLower.count("e")
LoveCount += SecondNameToLower.count("l")
LoveCount += SecondNameToLower.count("o")
LoveCount += SecondNameToLower.count("v")
LoveCount += SecondNameToLower.count("e")

score = str(TrueCount) + str(LoveCount)
score = int(score)
if score < 10 or score > 90:
    print(f"Your score is {score}, you go together like coke and mentos.")
elif score >= 40 and score <= 50:
    print(f"Your score is {score}, you are alright together.")
else:
    print(f"Your score is {score}.")

# Tutaj kod ulepszony dodając pętle ( w kursie jeszcze nie ma pętli więc zostawiam jako komentarz)

# print("Welcome to the Love Calculator!")
# name1 = input("What is your name? \n")
# name2 = input("What is their name? \n")
#
# FirstNameToLower = name1.lower()
# SecondNameToLower = name2.lower()
#
# true_count = 0
# love_count = 0
#
# for name in [FirstNameToLower, SecondNameToLower]:
#     true_count += name.count("t")
#     true_count += name.count("r")
#     true_count += name.count("u")
#     true_count += name.count("e")
#     love_count += name.count("l")
#     love_count += name.count("o")
#     love_count += name.count("v")
#     love_count += name.count("e")
#
# score = str(true_count) + str(love_count)
# print(score)
