import random
rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

player_pick = int(input("What do you choose? Type 0 for Rock, 1 for Paper or 2 for Scissors.\n"))
possibilities = ['rock','paper','scissors']
random_possibility = random.choice(possibilities)
name_of_action = ""

if player_pick == 0:
    print(rock)
    print("rock")
elif player_pick == 1:
    print(paper)
    print("paper")
elif player_pick == 2:
    print(scissors)
    print("scissors")
else:
    print("Please pick 0 , 1 or 2")
    exit()

print("Computer choose:")
if random_possibility == 'rock':
    print(rock,"rock")
    random_possibility = 0
    name_of_action = 'rock'

elif random_possibility == 'paper':
    print(paper,"paper")
    random_possibility = 1
    name_of_action = 'paper'
else:
    print(scissors,"scissors")
    random_possibility = 2
    name_of_action = 'scissors'

if player_pick == random_possibility:
    print(f"Both of you pick {name_of_action} - DRAW!")
elif player_pick == 0:
    if random_possibility == 2:
        print("Rock smashes scissors! You win!")
    else:
        print("Paper covers rock! You lose.")
elif player_pick == 1:
    if random_possibility == 0:
        print("Paper covers rock! You win!")
    else:
        print("Scissors cuts paper! You lose.")
elif player_pick == 2:
    if random_possibility == 1:
        print("Scissors cuts paper! You win!")
    else:
        print("Rock smashes scissors! You lose.")