print("Welcome to the tip calculator!")
totalBill = float(input("What was the total bill? $"))
tip = int(input("How much tip would you like to give? 10%, 12%, or 15?% "))
people = int(input("How many people to split the bill? "))

# zamieniamy wartość napiwek z procentowej na liczbę zmiennoprzecinkową
tip = tip/100

# dodajemy napiwek do rachunku
totalCost = totalBill+(totalBill*tip)

# dzielimy koszt na liczbę uczestników
costPerPerson = totalCost / people

finalAmount = round(costPerPerson,2)
finalAmount = "{:.2f}".format(costPerPerson)
print(f"Each person should pay: ${finalAmount}")







